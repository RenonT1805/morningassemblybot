package main

import (
	"encoding/json"
	"io/ioutil"
	"morning-assembly-bot/config"
	"morning-assembly-bot/loggers"
	"morning-assembly-bot/utils"
	"os"
	"strconv"
	"time"
)

type userStat struct {
	Name         string `json:"Name"`
	Score        int    `json:"Score"`
	StopCounting bool   `json:"StopCounting"`
}

type channelStats struct {
	Stats map[string]userStat `json:"Stats"`
}

type stats struct {
	Channels           map[string]channelStats `json:"Channels"`
	LastTabulationDate string                  `json:"LastTabulationDate"`
}

func (s stats) showMessage(channelId string) string {
	cs, exist := s.Channels[channelId]
	if !exist || len(cs.Stats) == 0 {
		return "ここではまだ朝会の集計はしておらん。やるんなら徹底的にだ！"
	}
	message := "社会人は早起きじゃなきゃ価値なし！！\n"
	message += "現在の成績は以下の通りじゃ\n"
	for _, stat := range cs.Stats {
		if stat.StopCounting {
			continue
		}
		message += stat.Name + " の寝坊回数: " + strconv.Itoa(stat.Score) + " 回\n"
	}
	message += "本当に早起きを想うちょるんなら寝不足を晒すな…！"
	return message
}

func getStats() stats {
	logger := loggers.GetLogger()
	config := config.GetConfig()
	if config.App.StatsPath == "" {
		return stats{}
	}
	bytes, err := ioutil.ReadFile(config.App.StatsPath)
	if err != nil {
		logger.Warning(err)
		return stats{}
	}
	ret := stats{}
	if err := json.Unmarshal(bytes, &ret); err != nil {
		logger.Warning(err)
		return stats{}
	}
	if ret.Channels == nil {
		ret.Channels = map[string]channelStats{}
		ret.LastTabulationDate = utils.TimeToString(time.Now())
	}
	return ret
}

func saveStats(s stats) {
	config := config.GetConfig()
	bytes, err := json.Marshal(s)
	if err != nil {
		logger.Warning(err)
		return
	}
	if err := ioutil.WriteFile(config.App.StatsPath, bytes, os.ModePerm); err != nil {
		logger.Warning(err)
	}
}
