package main

import (
	"morning-assembly-bot/loggers"
	"os"

	"github.com/kardianos/service"
)

var logger service.Logger

func main() {
	svcConfig := &service.Config{
		Name:        "MorningAssemblyBot",
		DisplayName: "MorningAssemblyBot",
		Description: "MorningAssemblyBot",
	}

	app := &app{}
	app.exit = make(chan struct{})
	s, err := service.New(app, svcConfig)
	if err != nil {
		panic(err)
	}

	loggers.Initialize(s)
	logger = loggers.GetLogger()

	if len(os.Args) > 1 {
		err = service.Control(s, os.Args[1])
		if err != nil {
			logger.Info("Failed (%s) : %s\n", os.Args[1], err)
			return
		}
		logger.Info("Succeeded (%s)\n", os.Args[1])
	} else {
		s.Run()
	}
}
