package main

import (
	"morning-assembly-bot/config"
	"morning-assembly-bot/loggers"
	"morning-assembly-bot/utils"
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/kardianos/service"
)

type app struct {
	exit    chan struct{}
	discord *discordgo.Session
	logger  loggers.Logger
}

func (a *app) getMornigAssemblyChannel(guildId string) *discordgo.Channel {
	config := config.GetConfig()
	channels, err := a.discord.GuildChannels(guildId)
	if err != nil {
		return nil
	}
	for _, channel := range channels {
		if channel.Name == config.App.ChannelName && channel.Type == discordgo.ChannelTypeGuildText {
			return channel
		}
	}
	return nil
}

func (a *app) getLosers(guild *discordgo.Guild, cs channelStats) map[string]*discordgo.User {
	joinedUsers := map[string]struct{}{}
	for _, voiceJoinedUser := range guild.VoiceStates {
		user, err := a.discord.User(voiceJoinedUser.UserID)
		if err != nil {
			a.logger.Warning(err)
			continue
		}
		joinedUsers[user.ID] = struct{}{}
	}
	losers := map[string]*discordgo.User{}
	for _, member := range guild.Members {
		if _, exist := joinedUsers[member.User.ID]; !exist && !member.User.Bot {
			if cs.Stats[member.User.ID].StopCounting {
				continue
			}
			losers[member.User.ID] = member.User
		}
	}
	return losers
}

func (a *app) updateStats(losers map[string]*discordgo.User, channelId string, s stats) stats {
	lastTabulationTime := utils.StringToTime(s.LastTabulationDate)
	if lastTabulationTime.Month() != time.Now().Month() {
		message := "月が変わった。先月の戦績を公開する。\n"
		for key, item := range s.Channels[channelId].Stats {
			message += item.Name + " が寝坊した回数は " + strconv.Itoa(item.Score) + "回\n"
			item.Score = 0
			s.Channels[channelId].Stats[key] = item
		}
		message += "今後も引き続き早起きに努めい。"
		a.discord.ChannelMessageSend(channelId, message)
	}
	s.LastTabulationDate = utils.TimeToString(time.Now())
	for _, user := range losers {
		stat := s.Channels[channelId].Stats[user.ID]
		stat.Name = user.Username
		stat.Score++
		s.Channels[channelId].Stats[user.ID] = stat
	}
	saveStats(s)
	return s
}

func (a *app) greeting() {
	for _, guild := range a.discord.State.Guilds {
		stats := getStats()
		channel := a.getMornigAssemblyChannel(guild.ID)
		if channel == nil {
			return
		}
		if _, exits := stats.Channels[channel.ID]; !exits {
			stats.Channels[channel.ID] = channelStats{
				Stats: map[string]userStat{},
			}
		}
		losers := a.getLosers(guild, stats.Channels[channel.ID])
		stats = a.updateStats(losers, channel.ID, stats)
		if channel := a.getMornigAssemblyChannel(guild.ID); channel != nil {
			message := ""
			if len(losers) != 0 {
				message += "夜遅くまで起きて即寝坊とは…とんだ寝坊助の集まりじゃのう " + guild.Name + " 海賊団。生活リズムが生活リズム…それも仕方ねェか…！\n"
				for _, user := range losers {
					message += user.Username + "\n"
				}
				message += "こやつらは所詮…朝活時代の”敗北者”じゃけェ……！"
			} else {
				message += "堂々としちょるのう…早起きの息子…"
			}
			_, err := a.discord.ChannelMessageSend(channel.ID, message)
			if err != nil {
				a.logger.Warning(err)
			}
		}
	}
}

func (a *app) toggleCounting(user *discordgo.User, channelId string) {
	stats := getStats()
	if _, exits := stats.Channels[channelId]; !exits {
		stats.Channels[channelId] = channelStats{
			Stats: map[string]userStat{},
		}
	}
	if _, exits := stats.Channels[channelId].Stats[user.ID]; !exits {
		stats.Channels[channelId].Stats[user.ID] = userStat{
			Name:         user.Username,
			Score:        0,
			StopCounting: false,
		}
	}
	stat := stats.Channels[channelId].Stats[user.ID]
	stat.StopCounting = !stat.StopCounting
	if stat.StopCounting {
		a.discord.ChannelMessageSend(channelId, "自分の集計ストップとは…とんだ腰抜けじゃのう")
	} else {
		a.discord.ChannelMessageSend(channelId, "貴様の集計を再開する")
	}
	stats.Channels[channelId].Stats[user.ID] = stat
	saveStats(stats)
}

func (a *app) messageHnadler(s *discordgo.Session, m *discordgo.MessageCreate) {
	channel := a.getMornigAssemblyChannel(m.GuildID)
	if channel == nil || channel.ID != m.ChannelID || m.Author.Bot {
		return
	}
	switch m.Message.Content {
	case "stats":
		stats := getStats()
		a.discord.ChannelMessageSend(channel.ID, stats.showMessage(channel.ID))
	case "取り消せよ":
		a.discord.ChannelMessageSend(channel.ID, "取り消せだと？断じて取り消すつもりはない")
	case "ケケ":
		a.discord.ChannelMessageSend(channel.ID, "やつは今日もおらん。当たり前じゃろうが")
	case "わっしが行こうか？":
		a.discord.ChannelMessageSend(channel.ID, "待てボルサリーノ。夜更かしには未知の健康被害がある")
	case "toggle":
		a.toggleCounting(m.Author, channel.ID)
	default:
		s.ChannelMessageDelete(channel.ID, m.ID)
	}
}

func (a *app) run() {
	var err error
	a.discord, err = discordgo.New()
	if err != nil {
		panic(err)
	}
	config := config.GetConfig()
	a.discord.Token = config.Discord.Token
	a.discord.Identify.Intents = discordgo.IntentsAll
	a.discord.AddHandler(a.messageHnadler)
	a.discord.Open()
	updateFlag := true
	for {
		time.Sleep(3 * time.Second)
		now := utils.StringToTime(utils.TimeToString(time.Now()))
		wakeupTime := utils.StringToTime(utils.TimeToString(now)[:11] + config.App.WakeupTime)
		if wakeupTime.Before(now) {
			if updateFlag {
				a.greeting()
			}
			updateFlag = false
		} else {
			updateFlag = true
		}
	}
}

func (a *app) Start(s service.Service) error {
	a.logger = loggers.GetLogger()
	go a.run()
	return nil
}

func (a *app) Stop(s service.Service) error {
	close(a.exit)
	a.discord.Close()
	return nil
}
