package utils

import (
	"math/rand"
	"time"
)

var layout = "2006-01-02 15:04:05"

// Random 乱数生成
func Random(min, max int) int {
	return rand.Intn(max-(min-1)) + min
}

// TimeToString Time to string
func TimeToString(t time.Time) string {
	str := t.Format(layout)
	return str
}

// StringToTime String to time
func StringToTime(str string) time.Time {
	t, _ := time.Parse(layout, str)
	return t
}
