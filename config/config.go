package config

import (
	"io/ioutil"
	"os"
	"strings"

	"github.com/spf13/viper"
)

const CONFIG_FILENAME = "appconfig.json"
const DEFAULT_APP_PATH = "/etc/mabot"
const DEFAULT_CONFIG = `
{
	"Discord": {
		"Token": "your bot token"
	},
	"App": {
		"WakeUptime": "07:00:00",
		"StatsPath": "/etc/mabot/stats.json",
		"ChannelName": "朝会"
	}
}
`

type Config struct {
	Discord struct {
		Token string `json:"Token"`
	} `json:"Discord"`
	App struct {
		WakeupTime  string `json:"WakeupTime"`
		StatsPath   string `json:"StatsPath"`
		ChannelName string `json:"ChannelName"`
	} `json:"App"`
}

var config Config

func init() {
	_, err := os.Stat(GetAppDir() + "/" + CONFIG_FILENAME)
	if err != nil {
		if err := os.MkdirAll(GetAppDir(), os.ModePerm); err != nil {
			panic(err)
		}
		ioutil.WriteFile(GetAppDir()+"/"+CONFIG_FILENAME, []byte(DEFAULT_CONFIG), os.ModePerm)
	}
	v := viper.New()
	v.SetConfigFile(GetAppDir() + "/" + CONFIG_FILENAME)
	if err := v.ReadInConfig(); err != nil {
		panic(err)
	}
	if err := v.Unmarshal(&config); err != nil {
		panic(err)
	}
}

// GetAppDir Get app dir
func GetAppDir() string {
	dir := os.Getenv("MORNING_ASSEMBLY_BOT_DIR")
	if dir == "" {
		return DEFAULT_APP_PATH
	}
	if !strings.HasSuffix(dir, "/") && !strings.HasSuffix(dir, "\\") {
		dir += "/"
	}
	return dir
}

// GetConfig returns config
func GetConfig() Config {
	return config
}
